import React, {useState} from "react"

const Filter = ({filterTitle, filterData, onClickCallback, value}) => {
    const [showAll, setShowAll] = useState(false)
    let addShowMore = filterData.length > 10

    return (
        <div className="bg-white p-4">
            <h3 className="font-bold mb-2">{filterTitle.toUpperCase().split('_').join(" ")}</h3>
            {filterData.slice(0,10).map((data, index) => {
                return (
                    <div className="cursor-pointer" onClick={() => {onClickCallback(data.key)}} key={index}>
                        <span className={"pr-2 text-sm" + (value == data.key ? " font-semibold text-blue-400" : "")}>
                            {data.key}
                        </span>
                        <span className="text-sm text-gray-400">{new Intl.NumberFormat().format(data.doc_count)}</span>
                    </div>
                )
            })}
            {addShowMore ? (
                <div>
                    <a className="pr-2 text-sm text-blue-400" onClick={() => setShowAll(true)}>Show more</a>
                </div>
            ) : null}
            {showAll && false && (
            <div className="w-full bg-blend-overlay bg-gray-200 justify-center items-center">
                <div>MODAL</div>
            </div>
            )}
        </div>
    )
}

export default Filter