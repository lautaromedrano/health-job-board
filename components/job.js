const Job = ({data, onClickCallback, expanded}) => {
    const utc1 = Date.now()
    const utc2 = Date.parse(data.created)
    let daysOld = Math.floor((utc1 - utc2) / (1000 * 60 * 60 * 24 * 7))

    return (
        <div className="ml-4 pb-4 border-b border-gray-200">
            <div className="w-full flex flex-row mt-4 justify-center cursor-pointer" onClick={onClickCallback}>
                <div className="sm:w-3/4 flex flex-col">
                    <div className="font-bold">{data.job_title}</div>
                    <div>{[data.job_type, data.salary_range.join(' - ') + ' an hour', data.city].join(' | ')}</div>
                </div>
                <div className="sm:w-1/4 text-right">
                    <span>{daysOld + ' weeks old'}</span>
                </div>
            </div>
            {expanded &&
            <div className="w-full flex flex-row mt-4">
                <div className="sm:w-4/5 flex flex-col pr-20 space-y-4">
                    <div className="flex flex-row">
                        <div className="sm:w-1/2 font-bold">Department:</div>
                        <div className="sm:w-1/2">{data.department.join(', ')}</div>
                    </div>
                    <div className="flex flex-row">
                        <div className="sm:w-1/2 font-bold">Hours / shifts</div>
                        <div className="sm:w-1/2">{data.hours[0] + ' hours / ' + data.work_schedule}</div>
                    </div>
                    <div className="flex flex-row">
                        <div className="sm:w-1/2 font-bold">Summary</div>
                        <div className="sm:w-1/2">{data.description}</div>
                    </div>
                </div>
                <div className="sm:w-1/5 flex flex-col justify-center space-y-4">
                    <button className="bg-blue-400 hover:bg-blue-600 font-semibold text-white py-1 px-3 border border-blue-400 hover:border-transparent rounded mx-8">
                        JOB DETAILS
                    </button>
                    <button className="bg-transparent hover:bg-blue-400 text-blue-400 font-semibold hover:text-white py-1 px-3 border border-blue-400 hover:border-transparent rounded mx-8">
                        SAVE JOB
                    </button>
                </div>
            </div>
            }
        </div>
    )
}

export default Job
