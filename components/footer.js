const Footer = () => {
    return (
        <footer className="w-full pt-12 flex flex-col sm:flex-row justify-start p-6">
            <div className="w-full sm:w-1/2 pr-6 flex flex-col space-y-4">
                <h2 className="text-2xl font-bold">About us</h2>
                <p>We are a team of nurses, doctors, technologists and executives dedicated to help nurses find jobs that they love.</p>
                <p>All copyrights reserved &copy; 2020 - Health Explore</p>
            </div>
            <div className="w-full sm:w-1/4 pr-6 flex flex-col space-y-4">
                <h2 className="text-2xl font-bold">Sitemap</h2>
                <ul className="space-y-2">
                <li>Nurses</li>
                <li>Employers</li>
                <li>Social networking</li>
                <li>Jobs</li>
                </ul>
            </div>
            <div className="w-full sm:w-1/4 pr-6 flex flex-col space-y-4">
                <h2 className="text-2xl font-bold">Privacy</h2>
                <ul className="space-y-2">
                <li>Terms of use</li>
                <li>Privacy policy</li>
                <li>Cookie policy</li>
                </ul>
            </div>
        </footer>
    )
}

export default Footer
