import React, { useState } from "react"

import Job from "./job"

const Hospital = ({data, onClickCallback, expanded}) => {
    const [jobIndex, setJobIndex] = useState(-1)

    return (
        <div>
            <div className="ml-4 mb-4 flex flex-row items-center cursor-pointer" onClick={onClickCallback}>
                <div className="h-8 w-8 mr-4 p-2 rounded bg-blue-400 text-white font-bold shadow flex justify-center items-center">{data.name.split(' ').slice(0, 2).map((item) => item.charAt(0))}</div>
                <span>{data.items.length + ' jobs for ' + data.name}</span>
            </div>
            {expanded && data.items.map((jobData, index) => {
                return (
                    <Job
                        data={jobData}
                        onClickCallback={() => { index === jobIndex ? setJobIndex(-1) : setJobIndex(index)}}
                        expanded={jobIndex === index}
                        key={index}
                    />
                )
            })}
        </div>
    )
}

export default Hospital
