const Header = () => {
    return (
        <header className="w-full pt-12 flex flex-row justify-between items-center p-6">
            <div>
                <span className="text-blue-400 font-bold">HEALTH EXPLORE</span>
            </div>
            <div>
                <ul className="flex flex-row">
                <li className="pr-4">PROFILE</li>
                <li className="pr-4">JOBS</li>
                <li className="pr-4">PROFESSIONAL NETWORK</li>
                <li className="pr-4">LOUNGE</li>
                <li className="pr-4">SALARY</li>
                </ul>
            </div>
            <div>
                <button className="bg-transparent hover:bg-blue-400 text-blue-400 font-semibold hover:text-white py-1 px-3 border border-blue-400 hover:border-transparent rounded mr-2">
                    CREATE JOB
                </button>
                <span className="h-8 w-8 bg-blue-400 p-2 mx-2 rounded-full justify-center items-center text-white font-bold shadow">JO</span>
                <span className="pr-4">LOGOUT</span>
            </div>
        </header>
    )
}

export default Header
