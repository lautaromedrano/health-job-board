import React, { useState, useEffect, useLayoutEffect } from "react"

import Filter from './filter'
import Hospital from './hospital'

const JobBoard = () => {
    const [hospitalIndex, setHospitalIndex] = useState(-1)
    const [filters, setFilters] = useState([])
    const [jobs, setJobs] = useState([])
    const [orderBys, setOrderBys] = useState([])
    const [filterBys, setFilterBys] = useState([])
    const [lastRequestSent, setLastRequestSent] = useState(0)

    const onSearchInputChange = (e) => {
        const searchWords = document.getElementById('search').value
        addFilterBy('keywords', searchWords)
    }

    const addOrderBy = (order) => {
        if (orderBys.indexOf(order) > -1) {
            setOrderBys(orderBys.filter((orderBy) => orderBy !== order))
        } else {
            setOrderBys([...orderBys, order])
        }
    }

    const addFilterBy = (filterKey, filterValue) => {
        let existingFilter = filterBys.find(filter => filter.key === filterKey)
        if (existingFilter == undefined) {
            setFilterBys([...filterBys, {key: filterKey, value: filterValue}])
        } else if (existingFilter.value === filterValue || !filterValue) {
            setFilterBys([...filterBys.filter(filter => filter.key !== filterKey)])
        } else {
            setFilterBys([...filterBys.filter(filter => filter.key !== filterKey), {key: filterKey, value: filterValue}])
        }
    }

    useEffect(() => {
        fetch(process.env.NEXT_PUBLIC_API_URL + '/api/filters')
            .then(response => response.json())
            .then(data => setFilters(data))
    }, [])

    useEffect(() => {
        setLastRequestSent(lastRequestSent + 1)

        fetch(
            process.env.NEXT_PUBLIC_API_URL + '/api/jobs',
            {
                method: 'POST',
                body: JSON.stringify({orderBys: orderBys, filters: filterBys, reqNumber: lastRequestSent}),
                headers: {
                    'Content-Type': 'application/json'
                }
            }
        )
        .then(response => response.json())
        .then(data => data.reqNumber == lastRequestSent && setJobs(data.jobs))
    }, [orderBys, filterBys])

    return (
        <section className="p-4 bg-gray-50">
            <div className="bg-white flex items-center border-solid border-1">
                <input
                    className="w-full py-4 px-6 text-gray-700 leading-tight focus:outline-none"
                    id="search"
                    onKeyDown={e => onSearchInputChange(e)}
                    type="text"
                    placeholder="Search for any job, title, keywords or company"
                />
            </div>
            <div className="w-full flex flex-row space-y-4 mt-2 py-2">
                <sidebar className="w-full sm:w-1/4 pr-6 flex flex-col space-y-4 mt-4">
                    {Object.keys(filters).map((key, index) => {
                        return (
                            <Filter
                                filterTitle={key}
                                filterData={filters[key]}
                                onClickCallback={(filterValue) => addFilterBy(key, filterValue)}
                                value={filterBys.find(filter => filter.key == key)?.value}
                                key={index} />
                        )
                    })}
                </sidebar>
                <div className="w-full sm:w-3/4 pr-6 flex flex-col space-y-4 mt-4 py-2 bg-white">
                    <div className="m-4 flex flex-row justify-between">
                        <div>
                            <span className="font-bold">
                                {jobs.map(hospital => hospital.items.length).reduce((sum, current) => { return sum + current}, 0)}
                            </span>
                            <span>&nbsp;job postings</span>
                        </div>
                        <div className="flex flex-row">
                            <span className="text-gray-400">
                                Sort by&nbsp;
                            </span>
                            <ul className="cursor-pointer ml-4">
                                <li className={"inline-block mr-2" + (orderBys.includes('location') ? " font-semibold" : "")} onClick={() => addOrderBy('location')}>
                                    Location
                                </li>
                                <li className={"inline-block mr-2" + (orderBys.includes('role') ? " font-semibold" : "")} onClick={() => addOrderBy('role')}>
                                    Role
                                </li>
                                <li className={"inline-block mr-2" + (orderBys.includes('department') ? " font-semibold" : "")} onClick={() => addOrderBy('department')}>
                                    Department
                                </li>
                                <li className={"inline-block mr-2" + (orderBys.includes('education') ? " font-semibold" : "")} onClick={() => addOrderBy('education')}>
                                    Education
                                </li>
                                <li className={"inline-block mr-2" + (orderBys.includes('experience') ? " font-semibold" : "")} onClick={() => addOrderBy('experience')}>
                                    Experience
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div>
                        {jobs.map((hospitalData, index) => {
                            return (
                                <Hospital
                                    data={hospitalData}
                                    onClickCallback={() => { index === hospitalIndex ? setHospitalIndex(-1) : setHospitalIndex(index)}}
                                    expanded={hospitalIndex === index}
                                    key={index}
                                />
                            )
                        })}
                    </div>
                </div>
            </div>
        </section>
    )
}

export default JobBoard
