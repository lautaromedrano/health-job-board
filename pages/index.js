import Footer from '../components/footer'
import Header from '../components/header'
import JobBoard from '../components/jobBoard'

const Index = () => (
  <div className="container mx-auto">
    <Header />
    <JobBoard />
    <Footer />
  </div>
)
export default Index
