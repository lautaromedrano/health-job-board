import jobs from '../../data/jobs'

const isJobAllowed = (job, filters) => {
  for (let i = 0; i < filters.length; i++) {
    let {key, value} = filters[i]

    if (key === "keywords") {
      let searchWords = value.split(" ");
      let matches = false

      for (let i = 0; i < searchWords.length; i++) {
        if (JSON.stringify(job).toLowerCase().indexOf(searchWords[i].toLowerCase()) > -1) {
          matches = true
        }
      }
      if (!matches) {
        return false
      }
    } else {
      // Small trick to handle both single and multiple values
      let jobValue = [].concat(job[key])

      if (!jobValue.includes(value)) {
        return false
      }
    }
  }
  return true
}

const applyOrderBys = (jobs, orderBys) => {
  return jobs.sort((a, b) => {
    for (let i = 0; i < orderBys.length; i++) {
      let aValue = JSON.stringify([].concat(a[orderBys[i]]).sort())
      let bValue = JSON.stringify([].concat(b[orderBys[i]]).sort())

      if (aValue < bValue) {
        return -1
      }
      if (aValue > bValue) {
        return 1
      }
    }
    return -1
  })
}

const applyFiltersAndOrdersToHealthCenter = (healthCenter, filters, orders) => {
  healthCenter.items = applyOrderBys(healthCenter.items.filter(job => isJobAllowed(job, filters)), orders);
};

const applyFiltersAndOrders = (jobs, filters, orders) => {
  jobs.forEach(healthCenter => applyFiltersAndOrdersToHealthCenter(healthCenter, filters, orders))
  return jobs.filter(healthCenter => healthCenter.items.length > 0);
};

export default async (req, res) => {
  let filteredJobs = applyFiltersAndOrders(JSON.parse(JSON.stringify(jobs)), req.body.filters, req.body.orderBys)
  res.statusCode = 200

  // this timeout emulates unstable network connection, do not remove this one
  // you need to figure out how to guarantee that client side will render
  // correct results even if server-side can't finish replies in the right order
  await new Promise((resolve) => setTimeout(resolve, 1000 * Math.random()))


  res.json({jobs: filteredJobs, reqNumber: req.body.reqNumber})
}
